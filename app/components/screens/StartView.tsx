import React, { FC, useState } from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import Button from "../ui/Button";
import DropDown from "../ui/DropDown";
import Field from "../ui/Field";

const ViewMain = styled.View`
  max-height: 100%;
  height: 100%;
  margin: 20px;
  color: rgb(73, 73, 73);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-bottom: 90px;
  font-weight: 400;
  font-size: 1.2rem;
  color: #52595e;
`;

const StartView: FC = () => {
  const [name, setName] = useState<string>("");
  const startDialogHandler = async () => {};
  const options = ["Mangoes", "Apples", "Oranges"];
  return (
    <ViewMain>
      <Field
        val={name}
        placeholder='Enter Name'
        onChange={(val) => setName(val)}
      />
      <DropDown nameDropDown='Enter the subject of the message' options={options} />
      <Button title='Start Dialog' onPress={startDialogHandler} />
    </ViewMain>
  );
};

export default StartView;
